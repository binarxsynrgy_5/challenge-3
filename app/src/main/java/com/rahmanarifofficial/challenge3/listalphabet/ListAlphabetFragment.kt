package com.rahmanarifofficial.challenge3.listalphabet

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rahmanarifofficial.challenge3.R
import com.rahmanarifofficial.challenge3.data.DummyData
import com.rahmanarifofficial.challenge3.databinding.FragmentListAlphabetBinding
import com.rahmanarifofficial.challenge3.listwordalphabet.ListWordAlphabetFragment

class ListAlphabetFragment : Fragment() {

    private var _binding: FragmentListAlphabetBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: WordAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentListAlphabetBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupUI()
    }

    private fun setupUI() {
        adapter = WordAdapter(DummyData.listOfAlphabet(), onItemClick = { position, item ->
            //Untuk menavigasi antar fragment bisa menggunakan 2 opsi ini
            //OPSI 1 : Navigasi antar halaman fragment menggunakan navigasi component
            val bundle = Bundle()
            bundle.putString("KEYWORD", item)
            findNavController().navigate(R.id.listWordAlphabetFragment, bundle)
            //OPSI 2 : Navigasi antar halaman menggunakan replace fragment
            //goToListWordAlphabet()
        })
        binding.recyclerViewAlphabet.adapter = adapter
        binding.recyclerViewAlphabet.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun goToListWordAlphabet(){
        val transaction = requireActivity().supportFragmentManager.beginTransaction()
        transaction.replace(R.id.nav_host_fragment, ListWordAlphabetFragment())
        transaction.commit()
    }
}