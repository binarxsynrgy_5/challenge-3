package com.rahmanarifofficial.challenge3.listwordalphabet

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.rahmanarifofficial.challenge3.data.DummyData
import com.rahmanarifofficial.challenge3.databinding.FragmentListWordAlphabetBinding
import com.rahmanarifofficial.challenge3.listalphabet.WordAdapter

class ListWordAlphabetFragment : Fragment() {

    private var _binding: FragmentListWordAlphabetBinding? = null
    private val binding get() = _binding!!

    private lateinit var adapter: WordAdapter
    private var params = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentListWordAlphabetBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupData()
        setupUI()
    }

    private fun setupData(){
        params = arguments?.getString("KEYWORD").toString()
    }

    private fun setupUI(){
        adapter = WordAdapter(DummyData.listOfWord(params)){ index, item ->
            val intent = Intent(Intent.ACTION_VIEW)
            val url = "https://www.google.com/search?q=${item}"
            intent.data = Uri.parse(url)
            startActivity(intent)
        }
        binding.recyclerViewWordAlphabet.adapter = adapter
        binding.recyclerViewWordAlphabet.layoutManager = LinearLayoutManager(requireContext())
    }
}