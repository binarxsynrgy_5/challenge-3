package com.rahmanarifofficial.challenge3.listalphabet

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rahmanarifofficial.challenge3.databinding.ItemWordBinding

class WordAdapter(
    private val itemList: List<String>,
    private val onItemClick: (Int, String) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = ItemWordBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return DefaultViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is DefaultViewHolder -> {
                holder.bindItem(itemList[position], position, onItemClick)
            }
        }
    }

    override fun getItemCount(): Int = itemList.size
}

class DefaultViewHolder(val itemBinding: ItemWordBinding) :
    RecyclerView.ViewHolder(itemBinding.root) {
    fun bindItem(
        item: String,
        position: Int,
        onItemClick: (Int, String) -> Unit,
    ) {
        itemBinding.textAlphabet.text = item
        itemBinding.root.setOnClickListener {
            onItemClick(position, item)
        }
    }
}