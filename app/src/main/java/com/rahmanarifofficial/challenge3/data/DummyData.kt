package com.rahmanarifofficial.challenge3.data

object DummyData {

    fun listOfAlphabet(): List<String> {
        val list = mutableListOf<String>()
        for (char in 'A'..'Z') {
            list.add(char.toString())
        }
        return list
    }

    fun listOfWord(alphabet: String): List<String> {
        val words = mutableListOf(
            "ADVANTAGE", "VALET", "AWESOME",
            "ABSOLUTE", "BLAST", "HABITAT",
            "COACH", "ORGANIC", "PERCENT",
            "DECADES", "ORDERED", "UDDER",
            "MELEE", "NEIGHBOR", "PERFECT",
            "FAIRY", "WHARF", "SEAFOOD",
            "BLOGGING", "SINGE", "GAMIEST",
            "ACHIEVE", "HISTORY", "SHEEN",
            "CANDIDATES", "ACCESSIBLE", "JIFFY",
            "ENJOYABLE", "NINJA", "FAJITA",
            "THINKING", "RISKY", "KINDLY",
            "LUNAR", "BELIEVE", "VOLUMINOUS",
            "ANIMALS", "MANIA", "PLASMAS",
            "ANYTHING", "NOTHING", "LUNAR",
            "BECOME", "FLOWERY", "PROSE",
            "POPPY", "REPLACE", "WEBPAGE",
            "PIQUE", "QUARTER", "CAIQUES",
            "RETAINS", "INCUR", "OPERATE",
            "ASKED", "SIMPLE", "CHAOS",
            "TRAINER", "LEFTY", "BIRTHDATE",
            "CURRICULUM", "LUCKY", "POULTRY",
            "DISADVANTAGEOUS", "VAPOR", "HAVING",
            "FORWARDNESS", "BRAWN", "HIGHWAY",
            "EXHIBITION", "APPROXIMATE", "BORAX",
            "LYMPH", "PHYSICIAN", "JUICY",
            "OVERZEALOUS", "OPTIMIZED", "OZONE",
            "SQUIZ", "QUARTZ", "MEZQUITS",
            "JACQUARDS", "QUILLAJA", "JONQUIL",
            "QOPH", "QUOTH", "SQUISHY",
            "JAZZY", "ZABAJONE", "MUZJIK",
            "FUZZY", "SCUZZY", "STYLIZE",
            "VEXIL", "EXUVIA", "GLOVEBOX"
        )

        val list = mutableListOf<String>()
        for (word in words) {
            if (word.contains(alphabet)){
                list.add(word)
            }
        }
        return list
    }

}