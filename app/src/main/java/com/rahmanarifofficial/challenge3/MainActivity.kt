package com.rahmanarifofficial.challenge3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.rahmanarifofficial.challenge3.data.DummyData
import com.rahmanarifofficial.challenge3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}